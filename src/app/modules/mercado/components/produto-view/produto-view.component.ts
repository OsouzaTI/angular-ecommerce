import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Produto from 'src/app/interfaces/produto.types';
import { ProdutosService } from 'src/app/services/produtos.service';

@Component({
  selector: 'app-produto-view',
  templateUrl: './produto-view.component.html',
  styleUrls: ['./produto-view.component.css']
})
export class ProdutoViewComponent implements OnInit {

  private id !: number;
  public produto !: Produto;

  constructor(private route : ActivatedRoute, private produtoService : ProdutosService) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      
      this.id = params['id'];   

      // lendo produto
      this.produto = this.produtoService.getProdutoById(this.id);
      
    })
  }

}
