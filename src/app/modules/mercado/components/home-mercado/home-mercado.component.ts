import { Component, OnInit } from '@angular/core';
import Produto from 'src/app/interfaces/produto.types';
import { ProdutosService } from 'src/app/services/produtos.service';

@Component({
  selector: 'app-home-mercado',
  templateUrl: './home-mercado.component.html',
  styleUrls: ['./home-mercado.component.css']
})
export class HomeMercadoComponent implements OnInit {

  public produtos : Array<Produto> = [];

  constructor(private produtoService : ProdutosService) { }
  
  ngOnInit(): void {
    this.produtos = this.produtoService.getAllProdutos();
    console.log(this.produtos)
  }


}
