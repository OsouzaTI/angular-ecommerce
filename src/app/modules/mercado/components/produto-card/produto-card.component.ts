import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-produto-card',
  templateUrl: './produto-card.component.html',
  styleUrls: ['./produto-card.component.css']
})
export class ProdutoCardComponent {

  @Input() public id !: number;
  @Input() public nome !: string;
  @Input() public descricao !: string;
  @Input() public preco !: number;

  public owner : boolean = false;

}
