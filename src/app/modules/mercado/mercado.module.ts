import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeMercadoComponent } from './components/home-mercado/home-mercado.component';

// Material
import {MatCardModule} from '@angular/material/card'; 
import { MatIconModule } from '@angular/material/icon';
import { ProdutoCardComponent } from './components/produto-card/produto-card.component';
import { MatButtonModule } from '@angular/material/button';
import { ProdutoViewComponent } from './components/produto-view/produto-view.component';
import { AppRoutingModule } from 'src/app/app-routing.module';

@NgModule({
  declarations: [
    HomeMercadoComponent,
    ProdutoCardComponent,
    ProdutoViewComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule
  ],
  exports: [
    HomeMercadoComponent,
    ProdutoCardComponent
  ]
})
export class MercadoModule { }
