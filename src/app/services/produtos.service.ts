import { Injectable } from '@angular/core';
import Produto from '../interfaces/produto.types';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {

  private produtos: Array<Produto> = [
    {
        id: 1,
        nome: "Camiseta",
        descricao: "Camiseta de algodão",
        preco: 29.99,
        quantidade: 10
    },
    {
        id: 2,
        nome: "Calça Jeans",
        descricao: "Calça jeans masculina",
        preco: 79.99,
        quantidade: 5
    },
    {
        id: 3,
        nome: "Tênis",
        descricao: "Tênis esportivo",
        preco: 99.99,
        quantidade: 8
    },
    
  ];

  constructor() { }

  getAllProdutos() : Array<Produto> {
    return this.produtos;
  }

  getProdutoById(id : number) : Produto {
    return this.produtos.filter(produto => produto.id = id).at(0)!;
  }

}
