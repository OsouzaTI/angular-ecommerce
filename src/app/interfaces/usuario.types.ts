import Produto from "./produto.types";

export default interface Usuario {
    nome: string;
    login: string;
    carrinho: Array<Produto>
};