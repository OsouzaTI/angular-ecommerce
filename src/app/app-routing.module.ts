import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeUsuarioComponent } from './modules/usuario/components/home-usuario/home-usuario.component';
import { HomeMercadoComponent } from './modules/mercado/components/home-mercado/home-mercado.component';
import { LoginComponent } from './modules/usuario/components/login/login.component';
import { ProdutoViewComponent } from './modules/mercado/components/produto-view/produto-view.component';


// Usuarios modulo

const routes: Routes = [
  {path: 'usuario', component: HomeUsuarioComponent },
  {path: 'login', component: LoginComponent },
  {path: 'mercado', component: HomeMercadoComponent},
  {path: 'produto/:id', component: ProdutoViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
